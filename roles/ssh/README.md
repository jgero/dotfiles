# Ssh

This role generates the ssh key and sets up the ssh configuration. I use separate ssh key pairs for
some domains which are automatically set up when these domains are entered into the list in the
`keypairs_for_hosts.yml` file.
