# PATH ---------------------------------------------------------------------------------------------

# OTHER ENVIRONMENT VARIABLES ----------------------------------------------------------------------

# config home
export XDG_CONFIG_HOME=$HOME/.config/
# preferred container runtime (used in some of my makefiles)
export CONTAINER_RUNTIME=podman
# task config
export TASKRC=~/.config/.taskrc

