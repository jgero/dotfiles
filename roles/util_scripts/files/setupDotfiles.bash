#!/usr/bin/env bash

source "$HOME/repos/dotfiles/venv/bin/activate"
ansible-playbook -i "$HOME/repos/dotfiles/hosts" "$HOME/repos/dotfiles/dotfiles.yml" --ask-become-pass

while [ : ]; do sleep 1; done

