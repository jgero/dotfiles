#!/usr/bin/env bash

# get all downloaded wireguard profiles
profiles=`sudo ls /etc/wireguard`

# select profile and trim .conf
selected=`printf "$profiles" | sed 's/.conf//g' | fzf`

wg-quick up $selected
