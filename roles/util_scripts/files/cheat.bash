#!/usr/bin/env bash

# define languages and commands to search
languages=`echo "golang lua cpp c typescript javascript bash python css html" | tr ' ' '\n'`
commands=`echo "find man sed awk xargs cp mv tr ls grep rg ps kill less rm cat ssh chmod chown git podman" | tr ' ' '\n'`

# select topic and get query
selected=`printf "$languages\n$commands" | fzf`
read -p "query: " query
query=`echo $query | tr ' ' '+'`

if printf "%s" "$languages" | grep -qs "$selected"; then
    tmux neww bash -c "curl 'cht.sh/$selected/$query' & while [ : ]; do sleep 1; done"
else
    tmux neww bash -c "curl cht.sh/$selected~$query & while [ : ]; do sleep 1; done"
fi
