#! /bin/bash

HAS_BIB=0

while getopts ':b' OPTION; do
	case "$OPTION" in
		b)
			HAS_BIB=1 >&2
			;;
		\?)
			echo "Invalid option: -$OPTARG" >&2
			exit 1
			;;
		:)
			echo "Option -$OPTARG requires an argument" >&2
			exit 1
			;;
	esac
done

# shift away all the options
shift $((OPTIND-1))

if [[ $HAS_BIB = 1 ]]; then
	toolbox run --container tex xelatex -interaction=nonstopmode -output-directory=out $@
	toolbox run --container tex biber --output-directory=out $@
	toolbox run --container tex xelatex -interaction=nonstopmode -output-directory=out $@
	toolbox run --container tex xelatex -interaction=nonstopmode -output-directory=out $@
else
	toolbox run --container tex xelatex -interaction=nonstopmode -output-directory=out $@
	toolbox run --container tex xelatex -interaction=nonstopmode -output-directory=out $@
fi
