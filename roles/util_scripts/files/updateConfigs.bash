#!/usr/bin/env bash

source "$HOME/repos/dotfiles/venv/bin/activate"
ansible-playbook -i "$HOME/repos/dotfiles/hosts" "$HOME/repos/dotfiles/dotfiles.yml" --tags "configuration"

while [ : ]; do sleep 1; done

