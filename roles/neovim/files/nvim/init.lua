local fn = vim.fn
local install_path = fn.stdpath('data') .. '/site/pack/packer/start/packer.nvim'

-- clone packer if it is missing
if fn.empty(fn.glob(install_path)) > 0 then
	fn.system({
		'git',
		'clone',
		'--depth',
		'1',
		'https://github.com/wbthomason/packer.nvim',
		install_path,
	})
	require('jgero.plugins').install(true)
	-- close packer once everything is done
	vim.api.nvim_create_autocmd({ 'User' }, {
		pattern = 'PackerComplete',
		once = true,
		command = 'quitall',
	})
	require('packer').sync()
else
	-- import my config
	require('jgero')
end
