local M = {}

local lspconfig = require('lspconfig')
local lspconfig_utils = require('jgero.config.lsp')
local lspcontainers = require('lspcontainers')

M.setup = function(wanted_servers)
	lspcontainers.setup({
		ensure_installed = wanted_servers,
		runtime = 'podman',
		network = 'slirp4netns',
	})

	for _, server in pairs(wanted_servers) do
		local container_opts = {}
		if server == 'gopls' then
			container_opts.image = 'localhost/go-lsp:latest'
			container_opts.wantNetwork = true
		end
		local config_opts = {
			capabilities = lspconfig_utils.capabilities,
			on_attach = lspconfig_utils.on_attach,
			cmd = lspcontainers.command(server, container_opts),
		}

		if server == 'sumneko_lua' then
			config_opts.settings = {
				Lua = {
					diagnostics = {
						globals = { 'vim' },
					},
					runtime = {
						version = 'LuaJIT',
						path = vim.split(package.path, ';'),
					},
					workspace = {
						library = {
							[vim.fn.expand('$VIMRUNTIME/lua')] = true,
							[vim.fn.expand('$VIMRUNTIME/lua/vim/lsp')] = true,
						},
					},
				},
			}
		end

		if server == 'svelte' or server == 'yamlls' or server == 'bashls' then
			config_opts.before_init = function(params)
				params.processId = vim.NIL
			end
		end
		if server == 'svelte' then
			config_opts.root_dir = lspconfig.util.root_pattern('package.json', '.git')
		end
		lspconfig[server].setup(config_opts)
	end
end

return M
