-- import plugins
require('jgero.plugins')
-- keybindings
require('jgero.config.keybindings')

local utils = require('jgero.utils')
local Job = require('plenary.job')

-- tab management
utils.opt('b', 'tabstop', 4)
utils.opt('b', 'softtabstop', 4)
utils.opt('b', 'shiftwidth', 4)
utils.opt('b', 'expandtab', false)
utils.opt('b', 'autoindent', true)
utils.opt('b', 'smartindent', true)

-- auto source vimrc from project dirs if they exist
utils.opt('o', 'exrc', true)

-- line numbers
utils.opt('w', 'number', true)
utils.opt('w', 'relativenumber', true)

-- nomally ignore case but do not ignore case when search is in uppercase
utils.opt('o', 'ignorecase', true)
utils.opt('o', 'smartcase', true)

-- keep buffers open
utils.opt('o', 'hidden', true)

utils.opt('w', 'wrap', false)
utils.opt('w', 'colorcolumn', '100')
-- this enables breaking lines with 'gq'
utils.opt('b', 'textwidth', 100)

-- history
utils.opt('o', 'backup', false)
utils.opt('o', 'swapfile', false)
utils.opt('o', 'undofile', true)

-- scrolling before cursor reaches the end
utils.opt('o', 'scrolloff', 8)

utils.opt('o', 'updatetime', 50)
utils.opt('o', 'completeopt', 'menu,menuone,noselect')
utils.opt('o', 'cmdheight', 2)

-- set splitting direction
utils.opt('o', 'splitright', true)
utils.opt('o', 'splitbelow', true)

-- set spellcheck for some files
vim.o.spelllang = 'en_us,de_de'
vim.api.nvim_create_autocmd({ 'BufEnter', 'BufWinEnter' }, {
	callback = function()
		if
			vim.bo.filetype == 'markdown'
			or vim.bo.filetype == 'gitcommit'
			or vim.bo.filetype == 'tex'
		then
			vim.opt_local.spell = true
		end
	end,
})

vim.o.termguicolors = true
vim.cmd([[colorscheme adwaita]])

local function update_theme(use_light_theme)
	if use_light_theme then
		vim.o.background = 'light'
	else
		vim.o.background = 'dark'
	end
end

-- sncronously initialize color-scheme
Job:new({
	command = 'gsettings',
	args = { 'get', 'org.gnome.desktop.interface', 'color-scheme' },
	on_exit = function(j, return_val)
		if return_val ~= 0 then
			print('gsettings returned error')
		end
		vim.defer_fn(function()
			vim.o.background = 'light'
			update_theme(string.find(j:result()[1], '\'default\''))
		end, 0)
	end,
}):sync()

-- watch for gsettgins changes in color scheme
Job:new({
	command = 'gsettings',
	args = { 'monitor', 'org.gnome.desktop.interface', 'color-scheme' },
	on_stdout = function(_, data)
		vim.defer_fn(function()
			update_theme(string.find(data, '\'default\''))
		end, 0)
	end,
}):start()
