local M = {}

-- The servers need to be in a global var because packer loses scope in 'config' functions. Local
-- vars do not work because under the hood packer converts the functions in confi to a string and
-- re-evaluates them later. This should be resolved once this is merged:
-- https://github.com/wbthomason/packer.nvim/pull/402
_G.wanted_lsp_servers = {
	'bashls',
	'gopls',
	'html',
	'svelte',
	'sumneko_lua',
	'tsserver',
	'yamlls',
}

local tsParsers = {
	'lua',
	'json',
	'yaml',
	'typescript',
	'javascript',
	'css',
	'html',
	'go',
	'svelte',
	'bash',
}

-- Packer bootstrap with parameter whether config should be applied.
-- When param is true this file will not require any other files from the config. That way only
-- installation tasks are done.
M.install = function(param)
	-- only run standalone when param is passed and it is boolean value 'true'
	local runStandalone = param or false == true

	require('packer').startup(function(use)
		use('wbthomason/packer.nvim')
		use('tpope/vim-surround')
		use('tpope/vim-repeat')

		-- navigation
		use({
			'nvim-telescope/telescope.nvim',
			requires = { { 'nvim-lua/plenary.nvim' } },
			config = function()
				if not runStandalone then
					require('jgero.config.telescope-config')
				end
			end,
		})
		use({ 'nvim-telescope/telescope-fzf-native.nvim', run = 'make' })
		use({
			'ThePrimeagen/harpoon',
			requires = {
				'nvim-lua/plenary.nvim',
			},
			config = function()
				require('harpoon').setup()
			end,
		})

		-- visuals
		use({
			'Mofiqul/adwaita.nvim',
			config = function()
				vim.cmd([[colorscheme adwaita]])
				vim.o.background = 'dark'
			end,
		})
		use({
			'rcarriga/nvim-notify',
			config = function()
				vim.notify = require('notify')
				if not runStandalone then
					require('jgero.config.lsp.notifications')
				end
			end,
		})

		-- git stuff
		use('tpope/vim-fugitive')
		use({
			'lewis6991/gitsigns.nvim',
			requires = {
				'nvim-lua/plenary.nvim',
			},
			config = function()
				if not runStandalone then
					require('jgero.config.git')
				end
			end,
		})

		-- general lsp
		use({
			'nvim-treesitter/nvim-treesitter',
			config = function()
				if not runStandalone then
					require('jgero.config.highlights')
				end
			end,
			run = function()
				if runStandalone then
					require('nvim-treesitter.install').update({ with_sync = true })(tsParsers)
				else
					require('nvim-treesitter.install').update()()
				end
			end,
		})
		use({
			'numToStr/Comment.nvim',
			config = function()
				if not runStandalone then
					require('jgero.config.context-comments')
				end
			end,
		})
		use({
			'JoosepAlviste/nvim-ts-context-commentstring',
			requires = { 'nvim-treesitter/nvim-treesitter', 'numToStr/Comment.nvim' },
		})
		use({
			'~/repos/lspcontainers.nvim',
			config = function()
				if not runStandalone then
					require('jgero.config.lsp.servers').setup(_G.wanted_lsp_servers)
				end
			end,
			run = function()
				require('lspcontainers').images_pull()
			end,
			requires = { 'neovim/nvim-lspconfig' },
		})
		use({
			'hrsh7th/nvim-cmp',
			config = function()
				if not runStandalone then
					require('jgero.config.lsp.completion')
				end
			end,
			requires = {
				--completion sources
				use({
					'hrsh7th/cmp-nvim-lua',
					'hrsh7th/cmp-nvim-lsp',
					'hrsh7th/cmp-buffer',
					'hrsh7th/cmp-path',
					'hrsh7th/cmp-cmdline',
					use({
						'ray-x/cmp-treesitter',
						requires = { 'nvim-treesitter/nvim-treesitter' },
					}),
				}),
				-- snippets
				use('l3mon4d3/luasnip'),
				use('saadparwaiz1/cmp_luasnip'),
			},
		})
		use({
			'windwp/nvim-autopairs',
			config = function()
				require('nvim-autopairs').setup({})
			end,
		})
	end)
end

return M
