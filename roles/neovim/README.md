# Neovim

## Setup

Depending on the `use_nvim_github_release` variable the play will install neovim as package from the
Fedora repositories or from the latest GitHub release. Either way the dependencies will be layered
and a toolbox for compiling treesitter parsers will be prepared. When the GitHub release is used the
play will download the appimage file from the release and put it into `~/.local/bin`.

## Config

TODO: describe my neovim configuration
