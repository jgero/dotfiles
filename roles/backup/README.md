# Backup Role

This role does the setup of the automatic backup to a backup drive and the remote repo with restic.
It also automatically restores from the last backup when first setting up the backup timers and
services.

> To make the restoring process work, when setting up the backup, the hard drive from which restic
> is supposed to restore has to be connected.

Then the backup will automatically run every Midnight and save to the remote repository. Because I
currently use sftp for the remote repository the ssh role is a dependency of this role.

The local backup will always run when the backup drive is plugged in.

## Error recovery

When encountering an error while trying to run the backup there are multiple mechanisms to try and
recover. The backup service itself re-tries every 30 seconds on error. Additionally it starts
another service that tries to unlock the repository to work around possible stale locks. If that
unlocking service fails as well a system notification is sent to notify the user that manual fixing
is required.

## Possible problems

Because it is quite the tedious process I could not completely confirm if the password setup and
automatic restoration process from a completely clean system works entirely without intervention.
