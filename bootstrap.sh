#!/usr/bin/env bash
set -e

# dotfiles' project root directory
DOTFILES="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

# install packages to run the ansible playbook
for packageName in ansible python3-psutil; do
	# install package if it's missing
	dnf list installed "$packageName" > /dev/null || sudo dnf install -y "$packageName"
done

# install community collection for dconf plugin
ansible-galaxy collection install community.general community.crypto

# run Ansible playbook using our user
ansible-playbook -i "$DOTFILES/hosts" "$DOTFILES/dotfiles.yml" --ask-become-pass

exit 0
